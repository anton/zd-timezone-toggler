# Zendesk timezone toggler

## Purpose

Toggles the timestamps between the current agent's local time and UTC.

![demo](assets/demo.gif)

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/anton/zd-timezone-toggler/-/raw/main/zd-timezone-toggler.js
